<?php

use App\Http\Controllers\FallbackController;
use App\Http\Controllers\FicheController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\FichePrepaController;
use App\Http\Controllers\FilterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

// what I add in web.php file hhhh that for me cuz I'm learning 

Route::get('/contact',function () {
  return view('contact');
})->middleware(['auth','verified'])->name('contact');

Route::resource('students',StudentController::class)
  ->only(['index','create','store','show','update','destroy'])
  ->middleware(['auth','verified']);

Route::resource('modules',ModuleController::class)
  ->only(['create','store','show','update','destroy','index'])
  ->middleware(['auth','verified']);



Route::resource('fiches',FicheController::class)
  ->only(['create','store','update','destroy','index','edit'])
  ->middleware(['auth','verified']);


Route::resource('groups',GroupController::class)
  ->only(['create','store','index'])
  ->middleware(['auth','verified']);

Route::get('/filter',FilterController::class)
  ->name('filter.index')
  ->middleware(['auth','verified']);

Route::fallback(FallbackController::class);
//-----------------------------------------------
Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
