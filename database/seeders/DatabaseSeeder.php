<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(2)->create();

        User::factory()->create([
            'name' => 'Abdessadek',
            'email' => 'fifalok42@gamil.com',
        ]);

        $this->call(ModuleSeeder::class);
        $this->call(GroupSeeder::class);
        $this->call(FicheSeeder::class);
    }
}
