<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            [
                'codeGroup' => '_201',
                'user_id' => 1,
            ],
            [
                'codeGroup' => '_202',
                'user_id' => 1,
            ],
            [
                'codeGroup' => 'DEV_101',
                'user_id' => 1,
            ],
            [
                'codeGroup' => 'DEV_102',
                'user_id' => 1,
            ]
        ];
        foreach ($groups as $key => $value){
            Group::create($value);
        }
    }
}
