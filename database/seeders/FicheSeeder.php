<?php

namespace Database\Seeders;

use App\Models\Fiche;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FicheSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fiches = [
            [
                'user_id' => 1,
                'module_id' => 4,
                'filiere' => 'Techniques de développement informatique',
                'group_id' => 2,
                'dureeTotalHour' => 2,
                'dureeTotalMin' => 30,
                'dureeRappel' => 20,
                'dureeElem' => 20,
                'dureePlanSeance' => 20,
                'dureeStrategies' => 20,
                'dureeDev' => 20,
                'dureeSythese' => 20,
                'dureeEvaluation' => 25,
                'dureeProchaineSeance' => 4,
                'dateSeance' => '2009-12-14',
                'objectifs' => 'La conception d\'une application hypermédia',
                'annee' => '1ér année',
                'rappel' => 'les concepts d\'une application hypermédia',
                'element' => 'L\'importance des applications web	',
                'planSeance' => 'Fonctionnalités d\'une application hypermédia Etablir un système de navigation Charte graphique Cahier de charges',
                'strategies' => 'Méthode exposition + Support de cour + Présentation',
                'developpement' => 'type objectifs population cible Spécifications techniques et fonctionnelles. organigramme barres de navigation choix des couleurs',
                'sythese' => 'Etablir le cahier des charges fonctionnelles et techniques d\'une application web',
                'evaluation' => 'Question - réponse',
                'prochaineSeance' => 'Introduction au langage HTML',
            ],
        ];

        foreach ($fiches as $key => $value){
            Fiche::create($value);
        }
    }
}
