<?php

namespace Database\Seeders;

use App\Models\Module;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
            [
                'nombre' => 205,
                'name' => 'Backend Laravel framework',
                'masse' => 180,
                'user_id' => 1,
            ],
            [
                'nombre' => 203,
                'name' => 'Frontend - Reactjs library',
                'masse' => 160,
                'user_id' => 1,
            ],
            [
                'nombre' => 204,
                'name' => 'UI / UX Design',
                'masse' => 60,
                'user_id' => 1,
            ],

        ];
        foreach ($modules as $key => $value){
            Module::create($value);
        }
    }
}
