<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fiches', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('module_id');
            $table->integer('dureeTotalHour');
            $table->integer('dureeTotalMin');
            $table->string('filiere');
            $table->integer('dureeRappel');
            $table->integer('dureeElem');
            $table->integer('dureePlanSeance');
            $table->integer('dureeStrategies');
            $table->integer('dureeDev');
            $table->integer('dureeSythese');
            $table->integer('dureeEvaluation');
            $table->integer('dureeProchaineSeance');
            $table->date('dateSeance');
            $table->string('objectifs');
            $table->string('annee');
            $table->string('rappel');
            $table->string('element');
            $table->text('planSeance');
            $table->text('strategies');
            $table->text('developpement');
            $table->text('sythese');
            $table->string('evaluation');
            $table->string('prochaineSeance');
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');       
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fiches');
    }
};
