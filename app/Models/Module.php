<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
  use HasFactory;
  protected $fillable = [
    'nombre',
    'name',
    'masse',
    'user_id',
  ];
  public function user(){
    return $this->belongsTo(User::class);
  }
  public function fiches(){
    return $this->hasMany(Fiche::class);
  }
}
