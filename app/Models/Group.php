<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
  use HasFactory;
  protected $fillable = [
    'codeGroup',
    'user_id',
  ];
  public function user(){
    return $this->belongsTo(User::class);
  }
  public function fiches(){
    return $this->hasMany(Group::class);
  }

  public function students()
  {
    return $this->hasMany(Student::class);
  }
}
