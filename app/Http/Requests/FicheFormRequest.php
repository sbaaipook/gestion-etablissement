<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FicheFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'module_id' => 'required',
            'filiere' => 'required',
            'group_id' => 'required',
            'dureeTotalHour' => 'required',
            'dureeTotalMin' => 'required',
            'dureeRappel' => 'required',
            'dureeElem' => 'required',
            'dureePlanSeance' => 'required',
            'dureeStrategies' => 'required',
            'dureeDev' => 'required',
            'dureeSythese' => 'required',
            'dureeEvaluation' => 'required',
            'dureeProchaineSeance' => 'required',
            'dateSeance' => 'required',
            'objectifs' => 'required',
            'annee' => 'required',
            'rappel' => 'required',
            'element' => 'required',
            'planSeance' => 'required',
            'strategies' => 'required',
            'developpement' => 'required',
            'sythese' => 'required',
            'evaluation' => 'required',
            'prochaineSeance' => 'required',
        ];
    }
}
