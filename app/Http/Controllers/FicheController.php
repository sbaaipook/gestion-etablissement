<?php

namespace App\Http\Controllers;

use App\Http\Requests\FicheFormRequest;
use App\Models\Fiche;
use App\Models\Group;
use App\Models\Module;
use Illuminate\Http\Request;

class FicheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fiches.index',[
            "fiches" => Fiche::with('user')
              ->orderBy('created_at')
              ->get(),
      
            "modules" => Module::with('user')
              ->latest()
              ->get(),
      
            "groups" => Group::with('user')
              ->latest()
              ->get(),
          ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fiches.create',[
            'modules'=> Module::with('user')->latest()->get(),
            'groups' => Group::with('user')->latest()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FicheFormRequest $request)
    {
        $validated = $request->validated(); 
          $sumDurations = $request->dureeDev 
            + $request->dureeElem 
            + $request->dureeRappel 
            + $request->dureeSythese 
            + $request->dureeStrategies 
            + $request->dureeEvaluation 
            + $request->dureePlanSeance 
            + $request->dureeProchaineSeance;
      
          if ($sumDurations === ($this->hoursToMinutes($request->dureeTotalHour) + $request->dureeTotalMin )){
            $request->user()->fiches()->create($validated);
            return redirect(route('fiches.index'))
              ->with('mesg','Un nouveau fiche ajouter !');
          }
          return redirect(route('fiches.create'))
            ->with('mesg', 'Durée total non equvalent la somme des durées'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fiche  $fiche
     * @return \Illuminate\Http\Response
     */
    public function show(Fiche $fiche)
    {
        // return view('fiches.show',[
        //     'fiche' => $fiche,
        //     'modules'=> Module::with('user')->latest()->get(),
        //     'groups' => Group::with('user')->latest()->get(),
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fiche  $fiche
     * @return \Illuminate\Http\Response
     */
    public function edit(Fiche $fich)
    {
        $this->authorize('update', $fich);
        return view('fiches.edit',[
            'fiche' => $fich,
            'modules'=> Module::with('user')->latest()->get(),
            'groups' => Group::with('user')->latest()->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Fiche  $fiche
     * @return \Illuminate\Http\Response
     */
    public function update(FicheFormRequest $request, Fiche $fich)
    {
        $this->authorize('update', $fich);
        $validated = $request->validated(); 

        $fich->update($validated);
        return redirect(route('fiches.index'))
            ->with('mesgedit','le fiche de la date '.$request->dateSeance.' est modifier !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fiche  $fiche
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fiche $fich)
    {
        $this->authorize('delete',$fich);
        $delete = $fich->dateSeance;
        $fich->delete();
        return redirect()
            ->route('fiches.index')
            ->with('delete','Le fiche de la date '.$delete.' est supprimer !');
    }

    //____helpers methods
    
    private function hoursToMinutes($hours){
        return $hours * 60;
    }
}
