<?php

namespace App\Http\Controllers;

use App\Models\Module;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if (Module::all()){
        return view('modules.create',[
          'modules' => Module::with('user')
            ->latest()
            ->get(),
        ]);
      }
      return view('modules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validated = $request->validate([
        'nombre' => 'required|integer|digits:3',
        'name' => 'required|string|max:255',
        'masse' => 'required|integer|min:10|max:300',
      ]);

      $request->user()->modules()->create($validated);
      $notif = 'Ajouter un module valider !';
      return redirect()->route('modules.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
      $this->authorize('update',$module);
      return view('modules.show',['module'=>$module]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        $this->authorize('update',$module);
        $validated = $request->validate([
          'nombre' => 'required|integer|digits:3',
          'name' => 'required|string|max:255',
          'masse' => 'required|integer|min:10|max:300',
        ]);  
        $module->update($validated);
        return redirect(route('modules.create'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        //
    }
}
