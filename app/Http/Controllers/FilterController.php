<?php

namespace App\Http\Controllers;

use App\Models\Fiche;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $module_id = $request->module_id;
        $group_id = $request->group_id;
        
        return view('filter.index',[
            'fiches' => Fiche::with('user')
                ->where('module_id',$module_id)
                ->where('group_id',$group_id)
                ->get()
        ]);
    }
}
