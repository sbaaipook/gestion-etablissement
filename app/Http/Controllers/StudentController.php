<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('students.index',[
        'students' => Student::with('user')
          ->latest()
          ->get(),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create',[
          'groups' => Group::with('user')->latest()->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validated = $request->validate([
        'nom' => 'required|string|max:255',
        'email' => 'required|string|max:255',
        'phone' => 'required|integer|digits:9',
        'section' => 'required|string',
        'image' => 'required|image|mimes:jpg,png,jpeg',
        'group_id' => 'required|integer',
      ]);
      $new_image_name = uniqid().'-'.$request->nom.'.'.$request->image->extension();
      $request->image->move(public_path('images'),$new_image_name);
      $validated['image'] = $new_image_name;
      $request->user()->students()->create($validated); 

      return redirect(route('students.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
      $this->authorize('update',$student);
      return view('students.show',[
        'student' => $student,
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
      $this->authorize('update',$student);
      $validated = $request->validate([
        'nom' => 'required|string|max:255',
        'email' => 'required|string|max:255',
        'phone' => 'required|integer|digits:9',
        'section' => 'required|string',
        'image' => '',
        'goup_id' =>''
      ]);

      if ($request->image !== null) {
        $new_image_name = uniqid().'-'.$request->nom.'.'.$request->image->extension();
        $request->image->move(public_path('images'),$new_image_name);
        $validated['image'] = $new_image_name;
        $validated['group_id'] = $student->group_id;
        $student->update($validated);
        return redirect(route('students.index'));
      }
      else {
        $validated['image'] = $student->image;
        $validated['group_id'] = $student->group_id;
        $student->update($validated);
      
        return redirect(route('students.index'));
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
      $this->authorize('delete',$student);
      $student->delete();
      return redirect(route('students.index'));
    }
}
