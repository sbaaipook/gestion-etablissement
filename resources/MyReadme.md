









## Create a trigger for increment the number of student inside of group:

```
CREATE Trigger insert_student
AFTER INSERT
ON students
FOR EACH ROW
BEGIN

    UPDATE groups
    SET numberStudents = numberStudents + 1
    WHERE id = NEW.group_id;

END //

DELIMITER;
```