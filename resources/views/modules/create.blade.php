<x-app-layout>
  @if (isset($notif))
  <div class="bg-green-300 py-2 w-full">{{ $notif }}</div>
  @endif
  <div class="my-12 w-3/4 mx-auto">
    <h1 class="my-4 text-gray-500 text-2xl font-bold">Ajouter un module</h1>
    <form action="{{route('modules.store')}}" method="POST">
       
      @csrf
      <div class="w-full my-2">
        <input 
          type="number" 
          placeholder="Nombre module" 
          name="nombre" 
          value="{{ old('nombre') }}"
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500" 
        />
        @error('nombre')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div> 

      <div class="w-full my-2">
        <input 
          type="text" 
          placeholder="Nom module" 
          name="name" 
          value="{{ old('name') }}"
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500" 
        />
        @error('name')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="w-full my-2">
        <input 
          type="number" 
          placeholder="Masse Heuraire" 
          name="masse" 
          value="{{ old('masse') }}"
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500" 
        />
        @error('masse')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div>
      
      <div>
        <x-primary-button>Ajouter</x-primary-button>
      </div>
    </form>
    @if (isset($modules))
    <div class="mt-12 w-full">
      <div class="flex bg-white p-2 my-2 rounded-md" >
        <span class="w-1/3 text-center text-xl font-bold">{{__('Module')}}</span>
        <span class="w-1/3 text-center text-xl font-bold">{{__('Libile')}}</span> 
        <span class="w-1/3 text-center text-xl font-bold">{{ __('Masse houraire') }}</span>
      </div>
      @foreach ($modules as $module)
      <div class="flex bg-white p-2 my-2 rounded-md" >
        <a href="{{route('modules.show', $module)}}" class="w-1/3 text-center text-xl">M{{ $module->nombre }}</a>
        <span class="w-1/3 text-center text-xl">{{ $module->name }}</span> 
        <span class="w-1/3 text-center text-xl">{{ $module->masse }}h</span>
      </div>
      @endforeach
    </div>
    @endif
  </div>
</x-app-layout>
