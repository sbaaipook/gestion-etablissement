<x-app-layout>
  <div class="my-12 w-3/4 mx-auto">
  <form action="{{route('modules.update',$module)}}" method="POST">
      @csrf
      @method('PUT')
      <div class="w-full my-2">
        <input 
          type="number" 
          placeholder="Nombre module" 
          name="nombre" 
          value="{{$module->nombre}}"
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500" 
        />
        @error('nombre')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div> 

      <div class="w-full my-2">
        <input 
          type="text" 
          placeholder="Nom module" 
          name="name" 
          value="{{ $module->name }}"
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500" 
        />
        @error('name')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="w-full my-2">
        <input 
          type="number" 
          placeholder="Masse Heuraire" 
          name="masse" 
          value="{{ $module->masse }}"
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500" 
        />
        @error('masse')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div>
      
      <div>
        <x-primary-button>Edit</x-primary-button>
      </div>
    </form>
    </div>
</x-app-layout>
