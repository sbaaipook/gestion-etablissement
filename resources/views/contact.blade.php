<x-app-layout>

  <div class="py-12 w-3/4 mx-auto">

    <h1 class="text-2xl text-gray-500 font-bold my-4">Contact</h1>
    <form action="{{route('contact')}}" method="GET" class="w-full">
      @csrf
      <div class="w-full my-2">
        <input class="w-full rounded-md px-2 py-1 transition duration-300 border border-gray-300" type="text" name="name" placeholder="Nom"/>
        @error('name')
        <div class="text-red-500 text-center my-2">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="w-full my-2">
        <input class="w-full rounded-md px-2 py-1 transition duration-300 border border-gray-300" type="text" placeholder="example@gmail.com" name="email"/>
        @error('email')
        <div class="text-red-500 text-center my-2">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="w-full my-2">
        <textarea class="w-full min-w-full px-2 py-1 max-w-full h-40 rounded-md transition duration-300 border border-gray-300"  name="description" placeholder="taper votre message.."></textarea>
        @error('description')
        <div class="text-red-500 text-center my-2">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div>
        <button class="px-4 py-1 text-gray-100 bg-green-500 hove:bg-green-600 rounded-md transition duration-300 border border-gray-300" type="submit">Envoyer</button>
      </div>
    </form>
  </div>
</x-app-layout>
