<x-app-layout>
  <div class="w-3/4 mx-auto my-12">
    <h1 class="text-2xl font-bold my-4">Ajouter un group</h1>
    <form action="{{route('groups.store')}}" method="POST">
      @csrf

      <div class="w-full my-2">
        <input 
          class="w-full rounded-md border-gray-300" 
          type="text" 
          name="codeGroup" 
          placeholder="Code de groupe"/>
          
      </div>

      <div>
        <x-primary-button>Ajouter</x-primary-button>
      </div>
    </form>

    <div class="my-10 p-4">
      @foreach ($groups as $group)
        <div class="flex justify-between bg-white py-2 px-4 rounded-md font-bold my-2">
          <span>Group:{{ $group->codeGroup }}</span> 
          <span>le nombers des étudiants :{{ $group->numberStudents }}</span> 

        </div>
      @endforeach
    </div>
  </div>
</x-app-layout>
