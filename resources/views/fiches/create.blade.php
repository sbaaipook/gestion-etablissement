<x-app-layout>
    @if (session()->has('mesg'))
    <h1 class="w-full p-2 bg-red-700 text-gray-100 font-bold text-xl text-center">Invalid!..</h1>
    <div class="bg-red-300 text-red-500 p-4 w-full font-bold text-center">
      {{ session()->get('mesg') }}
    </div>
    @endif
  
    <div class="w-3/4 mx-auto py-12">
      <h1 class="text-gray-600 font-bold text-2xl my-4">{{__('Ajouter une fiche de préparation')}} </h1>
      <form action="{{route('fiches.store')}}" method="POST">
        @csrf
        <div class="w-full my-2">
          <input 
            class="w-full border-gray-300 rounded-md" 
            type="text" 
            onfocus="this.type='date'" 
            name="dateSeance" 
            placeholder="La date de séance"
            value="{{old('dateSeance')}}"
          />
        </div>
        @error('dateSeance')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="w-full my-2 flex items-center gap-1">
          <input 
            class="w-1/2 border-gray-300 rounded-md" 
            type="number" 
            name="dureeTotalHour" 
            value="{{old('dureeTotalHour')}}"
            placeholder="Durée de la séance (heurs)" min="2" max="7"
          />
          <span class="text-3xl font-bold text-gray-400">:</span>
          <input 
            class="w-1/2 border-gray-300 rounded-md" 
            type="number" 
            name="dureeTotalMin" 
            placeholder="Durée de la séance (minutes)" min="0" max="59"
            value="{{old('dureeTotalMin')}}"
          />
        </div> 
        <div class="w-full">
            @error('dureeTotalHour')
                <div class="text-red-600 font-bold text-center py-2 my-2 bg-red-200">
                    {{ $message }}
                </div>
            @enderror
            @error('dureeTotalMin')
                <div class="text-red-600 font-bold text-center py-2 my-2 bg-red-200">
                    {{ $message }}                    
                </div>
            @enderror
        </div>
  
        <div class="w-full my-2">
          <input 
            class="w-full border-gray-300 rounded-md" 
            type="text" 
            name="filiere" 
            value="{{ old('filiere') }}"
            placeholder="Filière"/>
        </div> 
         @error('filiere')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="w-full my-2">
          <select 
            name="module_id"
            class="w-full border-gray-300 rounded-md" 
            onchange="let disOptionM = document.getElementById('module');disOptionM.disabled=true"
          >
            <option value="{{ old('module_id') ? old('module_id') : '' }}" id="module">{{ old('module_id') ? old('module_id') : __('Sélecsionnez le module') }}</option>
            @if (isset($modules))
            @foreach ($modules as $module)
            <option value="{{ $module->id }}">{{ $module->name }}</option>
            @endforeach
            @endif
          </select>
        </div>
         @error('module_id')
        <div class="bg-red-200 py-2 text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="w-full my-2">
          <select
            name="group_id"
            onchange="let disOption = document.getElementById('group');disOption.disabled=true" 
            class="w-full border-gray-300 rounded-md">
            <option value="{{ old('group_id') ? old('group_id') : ''}}" id="group">{{ old('group_id') ? old('group_id') : __('Sélecsionnez le group') }}</option>
            @if (isset($groups))
            @foreach ($groups as $group)
            <option value="{{$group->id}}">G-{{ $group->codeGroup }}</option>
            @endforeach
            @endif
          </select>
        </div>
         @error('group_id')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
                    
        <div class="border border-gray-300 p-3 bg-white rounded-md w-full my-2 flex justify-center items-center gap-2">
          <input 
            class="cursor-pointer w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" 
            type="radio" id="firstYear" value="1ér année" name="annee"/>
          
          <label 
            class="cursor-pointer ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            for="firstYear">1èr année</label>
          
          <input 
            class="cursor-pointer w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
            type="radio" id="secondYear" checked value="2ème année" name="annee"/>
          
          <label 
            class="cursor-pointer ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            for="secondYear">2ème année</label> 
        </div>
         @error('annee')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror 
    
        <div class="w-full">
          <input 
            class="w-full border-gray-300 rounded-md" 
            type="text" 
            name="objectifs" 
            placeholder="Objectifs de la séance"
            value="{{old('objectifs')}}"
          />
        </div>
         @error('objectifs')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <input 
            type="text" 
            name="rappel"
            value="{{old('rappel')}}"
            placeholder="Rappel"
            class="w-5/6 rounded-l-md border-gray-300"
          />
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeRappel"
            type="number" 
            value="{{old('dureeRappel')}}"
          />
        </div>
         @error('rappel')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <input 
            type="text" 
            name="element" 
            value="{{old('element')}}"
            placeholder="Eléments de motivation"
            class="w-5/6 rounded-l-md border-gray-300"
          />
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeElem"
            value="{{old('dureeElem')}}"
            type="number" 
          />
        </div>
         @error('element')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
                                    
        <div class="flex w-full my-2">
          <textarea  
            name="planSeance" 
            placeholder="Plan de la Séance"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{old('planSeance')}}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureePlanSeance"
            type="number" 
            value="{{old('dureePlanSeance')}}"
          />
        </div>
         @error('planSeance')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <textarea  
            name="strategies" 
            placeholder="Stratégies pédagogiques"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{old('strategies')}}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeStrategies"
            value="{{old('dureeStrategies')}}"
            type="number" 
          />
        </div>
         @error('strategies')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <textarea  
            name="developpement" 
            placeholder="Développement"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{old('developpement')}}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeDev"
            value="{{old('dureeDev')}}"
            type="number" 
          />
        </div>
         @error('developpement')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <textarea  
            name="sythese" 
            placeholder="Synthèse"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{old('sythese')}}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeSythese"
            type="number" 
            value="{{old('dureeSythese')}}"
          />
        </div>
         @error('sythese')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class=" my-2 w-full flex">
          <textarea  
            name="evaluation" 
            placeholder="Evaluation"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{old('evaluation')}}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeEvaluation"
            type="number" 
            value="{{old('dureeEvaluation')}}"
          />
        </div>
         @error('evaluation')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <input 
            type="text" 
            name="prochaineSeance"
            placeholder="Prochaine Séance"
            value="{{old('prochaineSeance')}}"
            class="w-5/6 rounded-l-md border-gray-300"
          />
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeProchaineSeance"
            type="number" 
            value="{{old('dureeProchaineSeance')}}"
          />
        </div>
        @error('prochaineSeance')
        <div class="text-red-600 text-center font-bold my-1 bg-red-200 py-2">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="w-full my-2">
          <x-primary-button>Enregistrer</x-primary-button>
        </div>
      </form>
    </div>
  </x-app-layout>
  