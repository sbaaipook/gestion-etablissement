<x-app-layout>
    <div class="w-3/4 mx-auto py-12">
      <h1 class="text-gray-600 font-bold text-2xl my-4">{{__('Ajouter une fiche de préparation')}} </h1>
      <form action="{{route('fiches.update',['fich'=>$fiche])}}" method="POST">
        @csrf
        @method('PATCH')
        <div class="w-full my-2">
          <input 
            class="w-full border-gray-300 rounded-md" 
            type="text" 
            onfocus="this.type='date'" 
            name="dateSeance" 
            placeholder="La date de séance"
            value="{{ old('dateSeance') ? old('dateSeance') : $fiche->dateSeance }}"
          />
        </div>
        @error('dateSeance')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="w-full my-2 flex items-center gap-1">
          <input 
            class="w-1/2 border-gray-300 rounded-md" 
            type="number" 
            name="dureeTotalHour" 
            value="{{ old('dureeTotalHour') ? old('dureeTotalHour') : $fiche->dureeTotalHour }}"
            placeholder="Durée de la séance (heurs)" min="2" max="7"
          />
          <span class="text-3xl font-bold text-gray-400">:</span>
          <input 
            class="w-1/2 border-gray-300 rounded-md" 
            type="number" 
            name="dureeTotalMin" 
            placeholder="Durée de la séance (minutes)" min="0" max="59"
            value="{{ old('dureeTotalMin') ? old('dureeTotalMin') : $fiche->dureeTotalMin }}"
          />
        </div> 
  
        <div class="w-full my-2">
          <input 
            class="w-full border-gray-300 rounded-md" 
            type="text" 
            name="filiere" 
            value="{{ old('filiere') ? old('filiere') : $fiche->filiere }}"
            placeholder="Filière"/>
        </div> 
         @error('filiere')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="w-full my-2">
          <select 
            name="module_id"
            class="w-full border-gray-300 rounded-md" 
            onchange="let disOptionM = document.getElementById('module');disOptionM.disabled=true"
          >
            <option 
              value="{{ old('module_id') ? old('module_id') : $fiche->module_id }}" 
              id="module">{{ old('module_id') ? old('module_id') : $fiche->module->name }}</option>
            @if (isset($modules))
            @foreach ($modules as $module)
            <option value="{{ $module->id }}">{{ $module->name }}</option>
            @endforeach
            @endif
          </select>
        </div>
         @error('module_id')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="w-full my-2">
          <select
            name="group_id"
            onchange="let disOption = document.getElementById('group');disOption.disabled=true" 
            class="w-full border-gray-300 rounded-md">
            <option 
              value="{{ old('group_id') ? old('group_id') : $fiche->group_id }}" 
              id="group">{{ !old('group_id') ? $fiche->group->codeGroup : __('Sélectionnez le groupe') }}</option>
            @if (isset($groups))
            @foreach ($groups as $group)
            <option value="{{$group->id}}">G-{{ $group->codeGroup }}</option>
            @endforeach
            @endif
          </select>
        </div>
         @error('group_id')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
                    
        <div class="border border-gray-300 p-3 bg-white rounded-md w-full my-2 flex justify-center items-center gap-2">
          <input 
            class="cursor-pointer w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" 
            type="radio" id="firstYear" value="1ér année" name="annee"/>
          
          <label 
            class="cursor-pointer ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            for="firstYear">1èr année</label>
          
          <input 
            class="cursor-pointer w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
            type="radio" id="secondYear" checked value="2ème année" name="annee"/>
          
          <label 
            class="cursor-pointer ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            for="secondYear">2ème année</label> 
        </div>
         @error('annee')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror 
    
        <div class="w-full">
          <input 
            class="w-full border-gray-300 rounded-md" 
            type="text" 
            name="objectifs" 
            placeholder="Objectifs de la séance"
            value="{{ old('objectifs') ? old('objectifs') : $fiche->objectifs }}"
          />
        </div>
         @error('objectifs')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <input 
            type="text" 
            name="rappel"
            value="{{ old('rappel') ? old('rappel') : $fiche->rappel }}"
            placeholder="Rappel"
            class="w-5/6 rounded-l-md border-gray-300"
          />
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeRappel"
            type="number" 
            value="{{ old('dureeRappel') ? old('dureeRappel') : $fiche->dureeRappel }}"
          />
        </div>
         @error('rappel')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <input 
            type="text" 
            name="element" 
            value="{{ old('element') ? old('element') : $fiche->element }}"
            placeholder="Eléments de motivation"
            class="w-5/6 rounded-l-md border-gray-300"
          />
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeElem"
            value="{{ old('dureeElem') ? old('dureeElem') : $fiche->dureeElem }}"
            type="number" 
          />
        </div>
         @error('element')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
                                    
        <div class="flex w-full my-2">
          <textarea  
            name="planSeance" 
            placeholder="Plan de la Séance"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{ old('planSeance') ? old('planSeance') : $fiche->planSeance }}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureePlanSeance"
            type="number" 
            value="{{ old('dureePlanSeance') ? old('dureePlanSeance') : $fiche->dureePlanSeance }}"
          />
        </div>
         @error('planSeance')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <textarea  
            name="strategies" 
            placeholder="Stratégies pédagogiques"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{ old('strategies') ? old('strategies') : $fiche->strategies }}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeStrategies"
            value="{{ old('dureeStrategies') ? old('dureeStrategies') : $fiche->dureeStrategies }}"
            type="number" 
          />
        </div>
         @error('strategies')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <textarea  
            name="developpement" 
            placeholder="Développement"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{ old('developpement') ? old('developpement') : $fiche->developpement }}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeDev"
            value="{{ old('dureeDev') ? old('dureeDev') : $fiche->dureeDev }}"
            type="number" 
          />
        </div>
         @error('developpement')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <textarea  
            name="sythese" 
            placeholder="Synthèse"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{ old('sythese') ? old('sythese') : $fiche->sythese }}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeSythese"
            type="number" 
            value="{{ old('dureeSythese') ? old('dureeSythese') : $fiche->dureeSythese }}"
          />
        </div>
         @error('sythese')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class=" my-2 w-full flex">
          <textarea  
            name="evaluation" 
            placeholder="Evaluation"
            class="w-5/6 rounded-l-md border-gray-300"
          >{{ old('evaluation') ? old('evaluation') : $fiche->evaluation }}</textarea>
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeEvaluation"
            type="number" 
            value="{{ old('dureeEvaluation') ? old('dureeEvaluation') : $fiche->dureeEvaluation }}"
          />
        </div>
         @error('evaluation')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="flex w-full my-2">
          <input 
            type="text" 
            name="prochaineSeance"
            placeholder="Prochaine Séance"
            value="{{ old('prochaineSeance') ? old('prochaineSeance') : $fiche->prochaineSeance }}"
            class="w-5/6 rounded-l-md border-gray-300"
          />
          <input 
            class="w-1/6 rounded-r-md border-l-0 border-gray-300"
            placeholder="Durée (min)"
            name="dureeProchaineSeance"
            type="number" 
            value="{{ old('dureeProchaineSeance') ? old('dureeProchaineSeance') : $fiche->dureeProchaineSeance }}"
          />
        </div>
        @error('prochaineSeance')
        <div class="text-red-600 text-center font-bold my-1">
          {{ $message }}  
        </div>
        @enderror
  
        <div class="w-full my-2">
          <button
            class="border border-gray-700 hover:border-0 px-4 py-2 w-40 rounded-md text-gray-700 font-bold hover:text-gray-100 hover:bg-blue-600 transition duration-500"
          >Modifer</button>

          

        </div>
      </form>

      <form
        action="{{ route('fiches.destroy',['fich' =>$fiche]) }}" 
        method="POST" id="supp">
        @csrf
        @method('DELETE')
        <button 
          type="submit"  
          class="border border-gray-700 hover:border-0  px-4 py-2 w-40 rounded-md text-gray-700 font-bold hover:text-gray-100 hover:bg-red-600 transition duration-500">
          Supprimer
        </button>
      </form>

      <script>
        let supp = document.getElementById('supp');
        supp.onsubmit=event=>{
          !confirm('vous-avez sûr pour supprimer le fiche de la date : {{ $fiche->dateSeance }}')?
          event.preventDefault(): '';
        }
      </script>
      
    </div>
  </x-app-layout>
  