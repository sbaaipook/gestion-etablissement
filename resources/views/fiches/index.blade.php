<x-app-layout>
    <!-- messages on screen -->

    @if (session()->has('delete'))
      <h1 class="w-3/4 p-2 bg-red-700 text-gray-100 font-bold text-xl text-center mt-2 rounded-t-md mx-auto">Worning..</h1>
      <div class="bg-red-300 text-red-600 text-center w-3/4 py-4 rounded-b-md mx-auto">
        {{ session()->get('delete') }}
      </div>
    @endif
    @if (session()->has('mesg'))
      <h1 class="w-3/4 p-2 bg-green-700 text-gray-100 font-bold text-xl text-center mt-2 rounded-t-md mx-auto">Success !</h1>
      <div class="bg-green-300 text-green-600 font-bold text-center w-3/4 py-4 rounded-b-md mx-auto">
        {{ session()->get('mesg') }}
      </div>
    @endif
    @if (session()->has('mesgedit'))
      <h1 class="w-3/4 p-2 bg-blue-700 text-gray-100 font-bold text-xl text-center mt-2 rounded-t-md mx-auto">Info !</h1>
      <div class="bg-blue-300 text-blue-600 font-bold text-center w-3/4 py-4 rounded-b-md mx-auto">
        {{ session()->get('mesgedit') }}
      </div>
    @endif

    <!-- button ajouter -->

    <div class="w-3/4 mx-auto my-5">
      <a 
        class="py-2 px-4 bg-green-600 text-gray-100 font-bold rounded-md hover:bg-green-700 transition duration-300"
        href="{{ route('fiches.create') }}">
        Ajouter nouveau fiche
      </a>
    </div>

    <!-- filter form -->

    @if (isset($modules) && isset($groups))
      <form 
        class="my-1 flex items-center mx-auto w-3/4 gap-1" 
        action="{{ route('filter.index') }}" 
        method="GET">
        
        @csrf
        <div class="my-2">
          <select 
            class="border border-gray-300 rounded-md w-full" 
            name="module_id">
            @foreach ($modules as $module)
              <option value="{{ $module->id }}">{{ $module->name }}</option>          
            @endforeach
          </select>
        </div>
  
        <div class="my-2">
          <select 
            class="border border-gray-300 rounded-md w-full"
            name="group_id">
            @foreach ($groups as $group)
              <option value="{{ $group->id }}">{{ $group->codeGroup }}</option>
            @endforeach
          </select>
        </div>
  
        <div>
          <input 
            class="cursor-pointer px-4 py-2 rounded-md bg-slate-500 hover:bg-slate-600 transition durations-400 text-gray-100"
            type="submit" 
            value="Filter">
        </div>
      </form>
      @endif
    

    <!-- all fiches -->
    <div class="w-3/4 mx-auto py-5 ">
      <hr>
      @if (isset($fiches))
      @forelse ($fiches as $fiche)
      <div class="bg-white p-4 rounded-md my-6 shadow shadow-gray-5OO shadow-2xl">
        <a 
          href="{{ route('fiches.edit',['fich'=> $fiche]) }}"
          class="block text-2xl font-bold my-4 mx-auto">Fiche de Préparation : {{ $fiche->dateSeance  }}</a>
  
        <!-- ------------------------------ -->
        <table class="w-full">
          <tr >
            <td class="border border-collapse border-gray-400">
              Date de la séance : {{ $fiche->dateSeance }}
            </td>
            <td class="border border-collapse border-gray-400 p-1">
              Durée de la séance : {{ $fiche->dureeTotalHour }}h{{ $fiche->dureeTotalMin }}
            </td>
          </tr>
          <tr>
            <td class="border border-collapse border-gray-400 p-1">
              Filière : {{ $fiche->filiere }}
            </td>
            <td class="border border-collapse border-gray-400 p-1">
              Anné : {{ $fiche->annee }}
            </td>
          </tr>
  
          <tr>
            <td class="border border-collapse border-gray-400 p-1">
              Module : {{ $fiche->module->name }}
            </td>
            <td class="border border-collapse border-gray-400 p-1">
              Group : {{ $fiche->group->codeGroup }}
            </td>
          </tr>
  
          <tr class="border border-collapse border-gray-400 p-1">
            <td colspan="1">
              Objectifs de la séance : {{ $fiche->objectifs }}
            </td>
          </tr>
        </table>
  
        <!-- ----------------------- -->
  
        <table class="w-full my-4">
          <thead>
              <th colspan="2" class="w-5/6 border border-collapse border-gray-400 p-1">Introduction</th>
              <th class="w-1/6 border border-collapse border-gray-400 p-1">Durée</th>          
          </thead>
          <tbody>
            <tr>
              <td class="w-1/6 border border-collapse border-gray-400 p-1">
                Rappel
              </td>
              <td class="w-3/6 border border-collapse border-gray-400 p-1">
                {{ $fiche->rappel }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeRappel }}min
              </td>
            </tr>
            
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Eléments de motivation
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->element }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeElem }}min
              </td>
            </tr>
  
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Plan de la Séance
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->planSeance }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureePlanSeance }}min
              </td>
            </tr>
          </tbody>
        </table>
  
        <!-- ----------------------- -->
  
        <table class="w-full my-4">
  
          <thead>
            <th class="border border-gray-400 p-1 w-1/6">
              Stratégies pédagogiques
            </th>
            <th class="border border-gray-400 p-1 w-1/6">
              Développement
            </th>
            <th class="border border-gray-400 p-1 w-4/6">
              Duée
            </th>
          </thead>
          <tbody>
            <tr>
              <td class="border border-gray-400 p-1 w-2/6">
                {{ $fiche->strategies }}
              </td>
              <td class="border border-gray-400 p-1 w-3/6">
                {{ $fiche->developpement }}
              </td>
              <td class="border border-gray-400 p-1 w-1/6">
                {{ ($fiche->dureeDev + $fiche->dureeStrategies) }}min
              </td>
            </tr>
          </tbody>
  
        </table>
  
        <table class="w-full my-4">
          <thead>
              <th colspan="2" class="w-5/6 border border-collapse border-gray-400 p-1">
                Conclusion
              </th>
              <th class="w-1/6 border border-collapse border-gray-400 p-1">Durée</th>          
          </thead>
          <tbody>
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Synthèse
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->sythese }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeSythese }}min
              </td>
            </tr>
            
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Evaluation
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->evaluation }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeEvaluation }}min
              </td>
            </tr>
  
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Prochain Séance
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->prochaineSeance }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeProchaineSeance }}min
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      @empty
      <div class="text-gray-700 text-2xl my-4">
        Non fiche pour affiche !
      </div>
      @endforelse
      @endif
    </div>
  
  </x-app-layout>
  