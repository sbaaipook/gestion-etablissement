<x-app-layout>
  <div class="w-3/4 mx-auto mt-10">
    <h1 class="text-2xl font-bold my-4">Ajouter un étudiant</h1>
    <div class="flex">
      <h1 class="text-xl font-bold px-8 w-1/6 text-center">Nom</h1>   
      <h1 class="text-xl font-bold px-8 w-1/6 text-center">E-mail</h1>
      <h1 class="text-xl font-bold px-8 w-1/6 text-center">Téléphone</h1>
      <h1 class="text-xl font-bold px-8 w-1/6 text-center">Rubrique</h1>
      <h1 class="text-xl font-bold px-8 w-1/6 text-center">Group</h1>
      <h1 class="text-xl font-bold px-8 text-center ">Image</h1> 
    </div>
    <div class="flex flex-col">
    @foreach ($students as $student)
    <div class="flex items-center my-2 p-2 bg-white rounded-md shadow">
      <p class="px-8 w-1/6 text-center">{{ ucfirst(trans($student->nom)) }}</p>
      <p class="px-8 w-1/6 text-center">{{ $student->email }}</p>
      <p class="px-8 w-1/6 text-center">+212 {{ $student->phone }}</p>
      <p class="px-8 w-1/6 text-center">{{ strtoupper($student->section) }}</p>
      <p class="px-8 w-1/6 text-center">G:{{ strtoupper($student->group->codeGroup) }}</p>
      <p class="px-8 w-1/6 text-center">
        <a href="{{route('students.show',$student)}}"><img class="object-cover w-10 h-10 rounded-full" src="/images/{{$student->image}}" alt="{{ $student->nom }}"/></a>
      </p>
    </div> 
    @endforeach
    </div>
    <div class="my-4">
      <a class="bg-green-500 p-2 rounded-md text-gray-100 hover:bg-green-600 transition duration-300" href="{{route('students.create')}}">Ajouter nouveau etudiant</a>
    </div>
  </div>
</x-app-layout>
