<x-app-layout>
  <div class="w-3/4 mx-auto mt-10">
    <h1 class="text-2xl font-bold my-4">Ajouter un étudiant</h1>
    <form action="{{ route('students.store') }}" method="POST" enctype="multipart/form-data">
      @csrf

      <div class="w-full my-2">
        <input 
          type="text" 
          placeholder="Nom" 
          name="nom" 
          value="{{ old('nom') }}"
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500" 
        />
        @error('nom')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="w-full my-2">
        <input 
          type="text" 
          placeholder="E-mail" 
          name="email" 
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500" 
          value="{{ old('email') }}"
        />
        @error('email')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div>


      <div class="w-full my-2">
        <div class="w-full flex items-center">
          <span class="border py-2 px-1 bg-white rounded-l-md border-r-0">+212</span>
          <input 
            type="number" 
            placeholder="Téléphone" 
            name="phone" 
            class="w-full form-inputi rounded-r-md border-gray-300 transition duration-500" 
            value="{{ old('phone') }}"
          />
        </div>
        @error('phone')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="w-full my-2">
        <select 
          name="section" 
          onchange="let none=document.querySelector('#none');none.innerHTML='sélectionnez votre rubrique';none.disabled=true;" 
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500"           
        > 
          <option value="{{ old('section') }}" id="none">@if (old('section')) {{ strtoupper(old('section')) }} @else {{ __('sélectionnez la rubrique') }} @endif</option>
          <option value="svt">SVT</option>
          <option value="pc">PC</option>
          <option value="mat">MAT</option>
        </select> 
        @error('section')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="w-full my-2">
        <select 
          name="group_id" 
          onchange="let disNone=document.querySelector('#disNone');disNone.innerHTML='sélectionnez votre rubrique';disNone.disabled=true;" 
          class="w-full form-inputi rounded-md border-gray-300 transition duration-500"           
        > 
          <option value="{{ old('group_id') }}" id="disNone">@if (old('group_id')) {{ strtoupper(old('group_id')) }} @else {{ __('sélectionnez le groupe') }} @endif</option>
          @foreach ($groups as $group)
            <option value="{{ $group->id }}">G{{ strtoupper($group->codeGroup) }}</option>
          @endforeach
        </select> 
        @error('group_id')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="w-full my-2">
        <label 
          class="w-full bg-gray-500 hover:bg-gray-700 transition duration-500 py-2 text-gray-100 block text-center rounded-md hover:scale-105 font-bold cursor-pointer" 
          for="image">
          Téléchargez votre image
        </label>
        <input 
          type="file" 
          placeholder="Phone" 
          name="image" 
          id="image"
          class="hidden"
          value="{{old('image')}}"
        />
        @error('image')
        <div class="text-red-600 text-center">
          {{ $message }}
        </div>
        @enderror
    </div>

    <div>
      <x-primary-button class="bg-green-500">Ajouter</x-app-layout>  
    </div>

    </form> 
  </div>
</x-app-layout>
