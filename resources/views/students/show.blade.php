<x-app-layout>
  <div class="relative w-3/4 mx-auto bg-white mt-20 rounded-xl p-4">
    <form action="{{route('students.update',$student)}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PATCH')
        <div class="flex flex-col items-center">
          <img class="w-40 h-40 rounded-full cursor-pointer transition durations-400" src="/images/{{$student->image}}" alt="{{$student->nom}}"/>

          <div class="w-1/4 text-center px-2 py-1 border border-gray-400 text-gray-400 my-4 block rounded-md">
            <label class="w-full px-2 py-1 cursor-pointer block" for="image">{{ __('Télechargez une image') }}</label>
            <input type="file" name="image" class="hidden" value="{{$student->image}}" id="image"/>
          </div>

        </div>
        <div class="w-full my-2">
          <input 
            type="text" 
            placeholder="Nom" 
            name="nom" 
            value="{{$student->nom}}"
            class="w-full form-input rounded-md border-gray-300 transition duration-500" 
          />
          @error('nom')
          <div class="text-red-600 text-center">
            {{ $message }}
          </div>
          @enderror
        </div>

        <div class="w-full my-2">
          <input 
            type="number" 
            placeholder="Télephone" 
            name="phone" 
            value="{{$student->phone}}"
            class="w-full form-input rounded-md border-gray-300 transition duration-500" 
          />
          @error('phone')
          <div class="text-red-600 text-center">
            {{ $message }}
          </div>
          @enderror
        </div>
        
        <div class="w-full my-2">
          <input 
            type="text" 
            placeholder="E-mail" 
            name="email" 
            value="{{$student->email}}"
            class="w-full form-input rounded-md border-gray-300 transition duration-500" 
          />
          @error('email')
          <div class="text-red-600 text-center">
            {{ $message }}
          </div>
          @enderror
        </div>

        <div class="w-full my-2">
          <select 
            name="section" 
            onchange="let none=document.querySelector('#none');none.innerHTML='sélectionnez votre rubrique';none.disabled=true;" 
            class="w-full form-input rounded-md border-gray-300 transition duration-500" 
          >
            <option value="{{$student->section}}" id="none">{{ strtoupper($student->section) }}</option>
            <option value="svt">SVT</option>
            <option value="pc">PC</option>
            <option value="mat">MAT</option>
          </select> 
          @error('section')
          <div class="text-red-600 text-center">
            {{ $message }}
          </div>
          @enderror
        </div>

        <div class="flex gap-4 justify-between items-center">
          <div class="flex gap-4">
            <input type="submit" class="bg-blue-500 px-7 py-2 text-gray-100 rounded-md hover:bg-blue-600" value="{{__('Modifier')}}"/> 
          </div>
          <div>
            <a href="{{route('students.index')}}" class="px-2 py-2 border border-gray-500 rounded-md">{{ __('Back to list') }}</a>   
          </div> 
        </div>
    </form>
    <form action="{{route('students.destroy', $student)}}" method="POST"  class="absolute top-2 right-2" id="del">
      @csrf
      @method('DELETE')
      <x-primary-button  class="border border-gray-500 rounded-md">{{ __('Supprimer') }}</x-primary-button>
    </form>
  </div>
  <script>
    let del = document.getElementById("del");
    del.onsubmit = (e) =>{
      if (!confirm('Êtes-vous sûr vouloir supprimer l\'etudiant : {{$student->nom}} ?')){
        e.preventDefault();
      }
    }
  </script>
</x-app-layout>
