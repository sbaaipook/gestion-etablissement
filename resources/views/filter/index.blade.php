<x-app-layout>
    @if (isset($fiches) && count($fiches)>0)
    <div class="w-3/4 mx-auto my-4 text-gray-700 font-bold">
      filter les fiche de préprations par 
      <span class="px-4 py-2 rounded-md bg-white">Group: {{ $fiches[0]->group->codeGroup }}</span>
      <span class="px-4 py-2 rounded-md bg-white">Module: {{ $fiches[0]->module->name }}</span>
    </div>
    @endif
    <div class="w-3/4 mx-auto my-10">
    @if (isset($fiches))
      @forelse ($fiches as $fiche)
      <div class="bg-white p-4 rounded-md my-6 shadow shadow-gray-5OO shadow-2xl">
        <a 
          href="{{ route('fiches.edit',['fich'=> $fiche]) }}"
          class="block text-2xl font-bold my-4 mx-auto">Fiche de Préparation : {{ $fiche->dateSeance  }}</a>
  
        <!-- ------------------------------ -->
        <table class="w-full">
          <tr >
            <td class="border border-collapse border-gray-400">
              Date de la séance : {{ $fiche->dateSeance }}
            </td>
            <td class="border border-collapse border-gray-400 p-1">
              Durée de la séance : {{ $fiche->dureeTotalHour }}h{{ $fiche->dureeTotalMin }}
            </td>
          </tr>
          <tr>
            <td class="border border-collapse border-gray-400 p-1">
              Filière : {{ $fiche->filiere }}
            </td>
            <td class="border border-collapse border-gray-400 p-1">
              Anné : {{ $fiche->annee }}
            </td>
          </tr>
  
          <tr>
            <td class="border border-collapse border-gray-400 p-1">
              Module : {{ $fiche->module->name }}
            </td>
            <td class="border border-collapse border-gray-400 p-1">
              Group : {{ $fiche->group->codeGroup }}
            </td>
          </tr>
  
          <tr class="border border-collapse border-gray-400 p-1">
            <td colspan="1">
              Objectifs de la séance : {{ $fiche->objectifs }}
            </td>
          </tr>
        </table>
  
        <!-- ----------------------- -->
  
        <table class="w-full my-4">
          <thead>
              <th colspan="2" class="w-5/6 border border-collapse border-gray-400 p-1">Introduction</th>
              <th class="w-1/6 border border-collapse border-gray-400 p-1">Durée</th>          
          </thead>
          <tbody>
            <tr>
              <td class="w-1/6 border border-collapse border-gray-400 p-1">
                Rappel
              </td>
              <td class="w-3/6 border border-collapse border-gray-400 p-1">
                {{ $fiche->rappel }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeRappel }}min
              </td>
            </tr>
            
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Eléments de motivation
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->element }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeElem }}min
              </td>
            </tr>
  
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Plan de la Séance
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->planSeance }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureePlanSeance }}min
              </td>
            </tr>
          </tbody>
        </table>
  
        <!-- ----------------------- -->
  
        <table class="w-full my-4">
  
          <thead>
            <th class="border border-gray-400 p-1 w-1/6">
              Stratégies pédagogiques
            </th>
            <th class="border border-gray-400 p-1 w-1/6">
              Développement
            </th>
            <th class="border border-gray-400 p-1 w-4/6">
              Duée
            </th>
          </thead>
          <tbody>
            <tr>
              <td class="border border-gray-400 p-1 w-2/6">
                {{ $fiche->strategies }}
              </td>
              <td class="border border-gray-400 p-1 w-3/6">
                {{ $fiche->developpement }}
              </td>
              <td class="border border-gray-400 p-1 w-1/6">
                {{ ($fiche->dureeDev + $fiche->dureeStrategies) }}min
              </td>
            </tr>
          </tbody>
  
        </table>
  
        <table class="w-full my-4">
          <thead>
              <th colspan="2" class="w-5/6 border border-collapse border-gray-400 p-1">
                Conclusion
              </th>
              <th class="w-1/6 border border-collapse border-gray-400 p-1">Durée</th>          
          </thead>
          <tbody>
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Synthèse
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->sythese }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeSythese }}min
              </td>
            </tr>
            
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Evaluation
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->evaluation }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeEvaluation }}min
              </td>
            </tr>
  
            <tr>
              <td class="w-1/5 border border-collapse border-gray-400 p-1">
                Prochain Séance
              </td>
              <td class="w-3/5 border border-collapse border-gray-400 p-1">
                {{ $fiche->prochaineSeance }}
              </td>
              <td class="border border-collapse border-gray-400 p-1">
                {{ $fiche->dureeProchaineSeance }}min
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      @empty
      <div class="text-gray-700 text-2xl my-4">
        Non fiche pour affiche ! <br>
        <a class="text-xl hover:text-blue-400 hover:underline transition durations-400" href="{{ route('fiches.index') }}">Retour a tous les fiches</a>
      </div>
      @endforelse
      @endif
    </div>

</x-app-layout>